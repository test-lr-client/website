import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom"
import { ArtiPage, AlgoPage } from "./pages"
import { Header } from './containers'

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <img src="/images/me_bottom.png" className="background" alt="leo le pogam" />
        <Switch>
          <Route path="/artisans">
            <ArtiPage />
          </Route>
          <Route path="/algorithmie">
            <AlgoPage />
          </Route>
          <Route>
            <Redirect to="/algorithmie" />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App