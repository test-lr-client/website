import React, { Component } from "react"
import { Link } from 'react-router-dom'
import { Tab } from '../../presentationals'
import './style.scss'

class Header extends Component {
    state = {
        menuIsVisible: false
    }

    toggleVisibilty = () => {
        this.setState(prevState => ({ menuIsVisible: !prevState.menuIsVisible }))
    }

    render() {
        return (
            <nav className="Header">
                <Link to="/" className="title-wrapper">
                    <img className="logo" src="/images/logo64.png" alt="logo" />
                    <div className="title">TEST LR CLIENT</div>
                </Link>
                <div className="spacer"></div>
                <div className="menu-burger" onClick={this.toggleVisibilty} style={{ backgroundImage: `url(/images/${this.state.menuIsVisible ? 'cross' : 'menu-burger'}.png)` }} />
                <div className={`tabs-wrapper${this.state.menuIsVisible ? ' visible' : ''}`}>
                    <Tab name="Algorithmie" onClick={this.toggleVisibilty} icon="" path="/algorithmie" />
                    <Tab name="Artisans" onClick={this.toggleVisibilty} icon="" path="/artisans" />
                    <Tab name="Projet sur Gitlab" onClick={this.toggleVisibilty} icon="" path="https://gitlab.com/test-lr-client" />
                </div>
            </nav>
        )
    }
}

export default Header