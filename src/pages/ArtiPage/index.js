import React, { Component } from "react"
import { Helmet } from "react-helmet"
import { Switch, Route, Redirect } from "react-router"
import { Tab } from '../../presentationals'
import ResultWrapper from "./ResultWrapper"
import SolutionWrapper from "./SolutionWrapper"
import SubjectWrapper from "./SubjectWrapper"

class ArtiPage extends Component {
    render() {
        return (
            <div className="page ArtiPage">
                <Helmet>
                    <title>Léo LE POGAM | Artisans</title>
                    <link rel="canonical" href="https://test-lr-client.leo-lepogam.playdeviant.com/artisans" />
                </Helmet>
                <div className="main-wrapper">
                    <div className="intern-nav">
                        <Tab name="Sujet" exact path="/artisans" />
                        <Tab name="Solution" exact path="/artisans/solution" />
                        <Tab name="Résultat" exact path="/artisans/resultat" />
                    </div>
                    <Switch>
                        <Route exact path="/artisans">
                            <SubjectWrapper />
                        </Route>
                        <Route exact path="/artisans/solution">
                            <SolutionWrapper />
                        </Route>
                        <Route exact path="/artisans/resultat">
                            <ResultWrapper />
                        </Route>
                        <Route path="/artisans/*">
                            <Redirect to={"/artisans"} />
                        </Route>
                    </Switch>
                </div>
            </div>
        )
    }
}

export default ArtiPage