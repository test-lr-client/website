import React, { Component } from 'react'
import { Table } from '../../presentationals'

class SubjectWrapper extends Component {
  state = {
    workers: [{ id: 1, name: 'SuperElec' }, { id: 2, name: 'MegaPool' }],
    reviews: [
      {
        id: 1,
        worker_id: 1,
        author: 'Paul',
        grade: 4,
        comment: 'Super electricien',
        review_date: new Date().toDateString(),
      },
      {
        id: 2,
        worker_id: 2,
        author: 'Michel',
        grade: 2,
        comment: 'Ouai bof',
        review_date: new Date().toDateString(),
      }
    ]
  }

  render() {
    return (
      <div className="wrapper subject">
        <h1>Le bouche à oreille des artisans en ligne</h1>
        <h2>Description</h2>
        <p>
          Nous avons deux tables en base de donnée:
        </p>
        <h3>Workers (`workers`)</h3>
        <Table headerTitles={[{ attribute: 'id', name: 'ID' }, { attribute: 'name', name: 'NAME' }]} content={this.state.workers} />
        <h3>UserReviews (`user_reviews` lié à la table workers par worker_id)</h3>
        <Table headerTitles={
          [
            { attribute: 'id', name: 'ID' },
            { attribute: 'worker_id', name: 'WORKER_ID' },
            { attribute: 'author', name: 'AUTHOR' },
            { attribute: 'grade', name: 'GRADE' },
            { attribute: 'comment', name: 'COMMENT' },
            { attribute: 'review_date', name: 'REVIEW DATE' },
          ]} content={this.state.reviews} />
        <h2>Objectif</h2>
        <p>
          Faire une requete SQL pour retourner les 3 <b>UserReviews</b> les plus récentes pour chacun des <b>Workers</b>.
        </p>
      </div>
    )
  }
}

export default SubjectWrapper