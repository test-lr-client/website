import React, { Component } from 'react'
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { dark } from 'react-syntax-highlighter/dist/esm/styles/prism';

class SolutionWrapper extends Component {
  codeLines = `is_friend("""
Benjamin est ami avec Paul
Sophie est amie avec moi
Je suis ami avec Benjamin
---
Est-ce que Sophie est mon amie ?""")  # return True;

is_friend("""
Benjamin est ami avec Paul
Frank est ami avec Paul
Mathieu est ami avec Aurore
Sophie est amie avec moi
Je suis ami avec Benjamin
---
Est-ce que Mathieu est mon ami ?""")  # return False;`

  render() {
    return (
      <div className="wrapper subject">
        <h1>Est-ce que 'X' est mon amie ?</h1>
        <h2>Description</h2>
        <p>
          Le programme prend une liste d'amitiée et une question en entrée.
          <br /><br />
          Le format est le suivant:<br />
          X lignes de lien d'amitiée <b>{"<nom1>"} est ami(e) avec {"<nom2>"}</b> ou  <b>je suis ami avec {"<nom2>"}</b> ou  <b>{"<nom1>"} est ami(e) avec moi</b><br />
          un séparateur <b>---</b><br />
          Une question <b>Est-ce que {"<nom>"} est mon ami(e)</b><br />
          <br />
          <b>
            Benjamin est ami avec Paul<br />
          Sophie est amie avec moi<br />
          Je suis ami avec Benjamin<br />
          ---<br />
          Est-ce que Sophie est mon amie ?<br />
          </b><br />
          Axiome: <b>l'ami de mon ami est mon ami</b><br />
          Le but est de répondre à la question par oui ou par non.
        </p>
        <h3>Notes</h3>
        <p>
          <i>Il y a unicitée des prénoms</i><br />
          <i>Les liens d'amitiés sont réciproques et transitifs</i>
        </p>
        <h2>Exemples</h2>
        <SyntaxHighlighter language="python" style={dark}>
          {this.codeLines}
        </SyntaxHighlighter>
      </div>
    )
  }
}

export default SolutionWrapper