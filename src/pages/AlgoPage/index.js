import React, { Component } from "react"
import { Helmet } from "react-helmet"
import { Switch, Route, Redirect } from "react-router"
import { Tab } from '../../presentationals'
import ResultWrapper from "./ResultWrapper"
import SolutionWrapper from "./SolutionWrapper"
import SubjectWrapper from "./SubjectWrapper"

class AlgoPage extends Component {
    render() {
        return (
            <div className="page AlgoPage">
                <Helmet>
                    <title>Léo LE POGAM | Algorithmie</title>
                    <link rel="canonical" href="https://test-lr-client.leo-lepogam.playdeviant.com" />
                </Helmet>
                <div className="main-wrapper">
                    <div className="intern-nav">
                        <Tab name="Sujet" exact path="/algorithmie" />
                        <Tab name="Solution" exact path="/algorithmie/solution" />
                        <Tab name="Résultat" exact path="/algorithmie/resultat" />
                    </div>
                    <Switch>
                        <Route exact path="/algorithmie">
                            <SubjectWrapper />
                        </Route>
                        <Route exact path="/algorithmie/solution">
                            <SolutionWrapper />
                        </Route>
                        <Route exact path="/algorithmie/resultat">
                            <ResultWrapper />
                        </Route>
                        <Route path="/algorithmie/*">
                            <Redirect to={"/algorithmie"} />
                        </Route>
                    </Switch>
                </div>
            </div>
        )
    }
}

export default AlgoPage