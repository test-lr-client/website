import React, { Component } from 'react'
import './style.scss'

class Table extends Component {
  get header() {
    if (!this.props.headerTitles) {
      return null
    }

    return (
      <div className="mt-header">
        {this.props.headerTitles.map(headerTitle => (<div className="mt-h-cell" key={headerTitle.attribute}>{headerTitle.name}</div>))}
      </div>
    )
  }

  get rows() {
    if (!this.props.content) {
      return null
    }

    return (
      <div className="mt-content">
        {this.props.content.map((row, rowIndex) => (
          <div className="mt-row" key={rowIndex}>
            {this.props.headerTitles.map((headerTitle, hIndex) => (
              <div className="mt-cell" key={hIndex + rowIndex * this.props.headerTitles.length}>
                {row[headerTitle.attribute] || ''}
              </div>
            ))}
          </div>
        ))}
      </div>
    )
  }

  render() {
    return (
      <div className="mt-wrapper">
        {this.header}
        {this.rows}
      </div>
    )
  }
}

export default Table