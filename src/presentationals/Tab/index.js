import React, { Component } from "react"
import { NavLink } from 'react-router-dom'
import './style.scss'

class Tab extends Component {
    get content() {
        return <>
            {this.props.icon ? <img src={this.props.icon} alt={this.props.name} /> : null}
            <div className="title">{this.props.name}</div>
            <div className="hover-line"></div>
        </>
    }

    render() {
        if (!this.props.path) {
            return null
        } else {
            if (/^http(s?):/.test(this.props.path)) {
                return (
                    <a href={this.props.path} target="_blank" rel="noreferrer" className="tab-wrapper">
                        {this.content}
                    </a>
                )
            } else {
                return (
                    <NavLink to={this.props.path} exact={this.props.exact} activeClassName="active" className="tab-wrapper">
                        {this.content}
                    </NavLink>
                )
            }
        }
    }
}

export default Tab